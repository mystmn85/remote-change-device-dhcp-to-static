﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Adding network class
using System.Net.NetworkInformation;
using System.Net;

// Adding PowerShell
using System.Diagnostics;


namespace ChangeDeviceToDhcpIP
{
    /*
     * Created by pcameron@mchs.com
     * This application does the following:
     * 1) See if the device is online and accessible. 
     * 2) Send a restart command with a delay timer. Once step three is sent the device changes IPs and will no longer receive commands
     * 3) Before the machine restarts this a PowerShell command is sent to change all network interfaces to DHCP
     */
    public partial class FormChangeToDHCP : Form
    {
        // This program creates a random ID on the form to show the program is operating and refreshing successfully. 
        static string randomNumbers()
        {
            var rand = new Random();
            var bytes = new byte[8];
            rand.NextBytes(bytes);
            return Convert.ToString(rand.Next(0, 500));

        }
        public FormChangeToDHCP()
        {
            InitializeComponent();

            textBoxRandomNumber.Text = randomNumbers();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            // Reset input fields
            string deviceName = textBoxDeviceName.Text;
            textBoxPingResults.Text = "";
            textBoxPullAddress.Text = "";
            textBoxRandomNumber.Text = randomNumbers();
            bool stepBystepConfirm = true;

            // Is the device form empty? true:false
            if (String.IsNullOrEmpty(deviceName))
            {
                MessageBox.Show($"The field Device Name is empty");
                stepBystepConfirm = false;
            }

            // confirm the device is online and pinging
            if (stepBystepConfirm)
            {
                bool checkedPing = checkingPing(deviceName);
                textBoxPingResults.Text = checkedPing ? "pinging" : "not online";
                if (!checkedPing)
                {
                    MessageBox.Show($"Unable to ping the device.");
                    stepBystepConfirm = false;
                }
            }

            // Second authentication by getting the IP address from the hostname
            if (stepBystepConfirm)
            {
                try
                {
                    string ipAddress = doGetHostAddresses(deviceName);
                    textBoxPullAddress.Text = ipAddress;

                }
                catch
                {
                    string ipAddress = "unable to pull the ip address from hostname";
                    textBoxPullAddress.Text = ipAddress;
                    stepBystepConfirm = false;
                }
            }
            /* 
            * Powershell :: send two commands at once
            * 1st will take the machine offline going from static IP to DHCP
            * 2nd command will restart the machine
            */

            if (stepBystepConfirm)
            {
                var restartedMachine = psUsingShutdownEXE(@"/m \\" + deviceName + " /r /t 20");
                var changedDHCP = psUsingPowershellEXE($"Get-WmiObject -Class Win32_NetworkAdapterConfiguration -Filter IPEnabled=true -ComputerName {deviceName} | ForEach-Object -Process {{$_.InvokeMethod('EnableDHCP', $null)}}");

                textBoxRestarted.Text = restartedMachine ? "...restarting in 10 seconds" : "not able to restart";
                textBoxdhcpChanges.Text = changedDHCP ? "yes" : "no";
            }
        }

        /* 
         * Using the "shutdown.exe command, we'll send a command to the remote device, delaying the restart 10 seconds.
         * It'll be plenty of time for the second command to be retrieved. 
         */
        static bool psUsingShutdownEXE(string commands)
        {   
            var message = false;

            try
            {
                using (Process ps = new Process())
                {
                    ps.StartInfo.RedirectStandardOutput = true;
                    ps.StartInfo.FileName = @"shutdown.exe";
                    ps.StartInfo.UseShellExecute = false;
                    ps.StartInfo.Arguments = commands;
                    ps.Start();

                    /* This can be used to output the PowerShell command. 
                    * string outputFromCommand = "";
                    * outputFromCommand = ps.StandardOutput.ReadToEnd();
                    */ 
                    message = true;
                    ps.WaitForExit();
                }
            }
            catch
            {
                message = false; 
            }

            return message;
        }
        static bool psUsingPowershellEXE(string commands)
        {
            bool message = false;

            // Link for PS :: https://duanenewman.net/blog/post/running-powershell-scripts-from-csharp/
            try { 
                using (Process ps = new Process())
                {
                    ps.StartInfo.RedirectStandardOutput = true;
                    ps.StartInfo.FileName = @"powershell.exe";
                    ps.StartInfo.UseShellExecute = false;
                    ps.StartInfo.Arguments =  commands;

                    /* If using Parallel pipelin to send both commands at the same time. This didn't work for me since one
                    * command used powershell.exe and the other uses shutdown.exe so it was ignored for now.
                    * ps.StartInfo.Arguments = $"{commands} | ForEach - Object - Parallel {{ $_ }}";
                    */
                    ps.Start();

                    /* This can be used to output the PowerShell command. 
                    * string outputFromCommand = "";
                    * outputFromCommand = ps.StandardOutput.ReadToEnd();
                    */
                    message = true;
                    ps.WaitForExit();
                }
            }
            catch
            {
                message = false;
            }

            return message;
        }

        // Pull down the IP address from the host name using Windows 10 class "using System.Net.NetworkInformation;"
        static string doGetHostAddresses(string hostname)
        {
            string finalName = string.Empty;

            IPAddress[] addresses = Dns.GetHostAddresses(hostname);

            foreach (IPAddress address in addresses)
            {
                finalName += address;
            }
            return finalName;
        }

        // .NET has a built in method called PING, which does just that"
        static bool checkingPing(string deviceName)
        {
            Ping pinger = null;
            bool deviceReply = false;

            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(deviceName);
                deviceReply = reply.Status == IPStatus.Success;
            }
            catch
            {
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }
            return deviceReply;
        }

        /*
        public static void DoGetHostAddresses(string hostname)
        {
            IPAddress[] addresses = Dns.GetHostAddresses(hostname);

            Console.WriteLine($"GetHostAddresses({hostname}) returns:");

            foreach (IPAddress address in addresses)
            {
                Console.WriteLine($"    {address}");
            }
        }
        */

        // Close the application when the "exit" button is clicked.
        private void buttonClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
