﻿
namespace ChangeDeviceToDhcpIP
{
    partial class FormChangeToDHCP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDeviceName = new System.Windows.Forms.TextBox();
            this.textBoxPingResults = new System.Windows.Forms.TextBox();
            this.textBoxPullAddress = new System.Windows.Forms.TextBox();
            this.labelDeviceName = new System.Windows.Forms.Label();
            this.labelPingResults = new System.Windows.Forms.Label();
            this.labelPullAddress = new System.Windows.Forms.Label();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.textBoxdhcpChanges = new System.Windows.Forms.TextBox();
            this.labeldhcpChanges = new System.Windows.Forms.Label();
            this.textBoxRandomNumber = new System.Windows.Forms.TextBox();
            this.textBoxRestarted = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelPersonalMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxDeviceName
            // 
            this.textBoxDeviceName.Location = new System.Drawing.Point(55, 77);
            this.textBoxDeviceName.Name = "textBoxDeviceName";
            this.textBoxDeviceName.Size = new System.Drawing.Size(206, 20);
            this.textBoxDeviceName.TabIndex = 0;
            // 
            // textBoxPingResults
            // 
            this.textBoxPingResults.Location = new System.Drawing.Point(55, 132);
            this.textBoxPingResults.Name = "textBoxPingResults";
            this.textBoxPingResults.ReadOnly = true;
            this.textBoxPingResults.Size = new System.Drawing.Size(206, 20);
            this.textBoxPingResults.TabIndex = 10;
            this.textBoxPingResults.TabStop = false;
            // 
            // textBoxPullAddress
            // 
            this.textBoxPullAddress.Location = new System.Drawing.Point(55, 189);
            this.textBoxPullAddress.Name = "textBoxPullAddress";
            this.textBoxPullAddress.ReadOnly = true;
            this.textBoxPullAddress.Size = new System.Drawing.Size(206, 20);
            this.textBoxPullAddress.TabIndex = 11;
            this.textBoxPullAddress.TabStop = false;
            // 
            // labelDeviceName
            // 
            this.labelDeviceName.AutoSize = true;
            this.labelDeviceName.Location = new System.Drawing.Point(55, 58);
            this.labelDeviceName.Name = "labelDeviceName";
            this.labelDeviceName.Size = new System.Drawing.Size(69, 13);
            this.labelDeviceName.TabIndex = 3;
            this.labelDeviceName.Text = "DeviceName";
            // 
            // labelPingResults
            // 
            this.labelPingResults.AutoSize = true;
            this.labelPingResults.Location = new System.Drawing.Point(55, 116);
            this.labelPingResults.Name = "labelPingResults";
            this.labelPingResults.Size = new System.Drawing.Size(72, 13);
            this.labelPingResults.TabIndex = 4;
            this.labelPingResults.Text = "Ping Callback";
            // 
            // labelPullAddress
            // 
            this.labelPullAddress.AutoSize = true;
            this.labelPullAddress.Location = new System.Drawing.Point(55, 173);
            this.labelPullAddress.Name = "labelPullAddress";
            this.labelPullAddress.Size = new System.Drawing.Size(102, 13);
            this.labelPullAddress.TabIndex = 5;
            this.labelPullAddress.Text = "Callback IP Address";
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(55, 415);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 1;
            this.buttonSubmit.Text = "Submit";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.buttonSubmit_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(667, 415);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 2;
            this.buttonClose.Text = "Exit";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // textBoxdhcpChanges
            // 
            this.textBoxdhcpChanges.Location = new System.Drawing.Point(58, 245);
            this.textBoxdhcpChanges.Name = "textBoxdhcpChanges";
            this.textBoxdhcpChanges.ReadOnly = true;
            this.textBoxdhcpChanges.Size = new System.Drawing.Size(203, 20);
            this.textBoxdhcpChanges.TabIndex = 12;
            this.textBoxdhcpChanges.TabStop = false;
            // 
            // labeldhcpChanges
            // 
            this.labeldhcpChanges.AutoSize = true;
            this.labeldhcpChanges.Location = new System.Drawing.Point(55, 229);
            this.labeldhcpChanges.Name = "labeldhcpChanges";
            this.labeldhcpChanges.Size = new System.Drawing.Size(148, 13);
            this.labeldhcpChanges.TabIndex = 9;
            this.labeldhcpChanges.Text = "Did the DHCP changes take?";
            // 
            // textBoxRandomNumber
            // 
            this.textBoxRandomNumber.Location = new System.Drawing.Point(691, 3);
            this.textBoxRandomNumber.Name = "textBoxRandomNumber";
            this.textBoxRandomNumber.ReadOnly = true;
            this.textBoxRandomNumber.Size = new System.Drawing.Size(97, 20);
            this.textBoxRandomNumber.TabIndex = 14;
            this.textBoxRandomNumber.TabStop = false;
            // 
            // textBoxRestarted
            // 
            this.textBoxRestarted.Location = new System.Drawing.Point(58, 298);
            this.textBoxRestarted.Name = "textBoxRestarted";
            this.textBoxRestarted.ReadOnly = true;
            this.textBoxRestarted.Size = new System.Drawing.Size(203, 20);
            this.textBoxRestarted.TabIndex = 13;
            this.textBoxRestarted.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 282);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Did the machine restart?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Created by: pcameron@mchs.com";
            // 
            // labelPersonalMessage
            // 
            this.labelPersonalMessage.AutoSize = true;
            this.labelPersonalMessage.Location = new System.Drawing.Point(55, 379);
            this.labelPersonalMessage.Name = "labelPersonalMessage";
            this.labelPersonalMessage.Size = new System.Drawing.Size(536, 13);
            this.labelPersonalMessage.TabIndex = 15;
            this.labelPersonalMessage.Text = "I recommend using PingInfoView for batch pinging. Giving good visual confirmation" +
    "s as devices are going offline.";
            // 
            // FormChangeToDHCP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelPersonalMessage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxRestarted);
            this.Controls.Add(this.textBoxRandomNumber);
            this.Controls.Add(this.labeldhcpChanges);
            this.Controls.Add(this.textBoxdhcpChanges);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.labelPullAddress);
            this.Controls.Add(this.labelPingResults);
            this.Controls.Add(this.labelDeviceName);
            this.Controls.Add(this.textBoxPullAddress);
            this.Controls.Add(this.textBoxPingResults);
            this.Controls.Add(this.textBoxDeviceName);
            this.Name = "FormChangeToDHCP";
            this.Text = "Change to IP to DHCP";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDeviceName;
        private System.Windows.Forms.TextBox textBoxPingResults;
        private System.Windows.Forms.TextBox textBoxPullAddress;
        private System.Windows.Forms.Label labelDeviceName;
        private System.Windows.Forms.Label labelPingResults;
        private System.Windows.Forms.Label labelPullAddress;
        private System.Windows.Forms.Button buttonSubmit;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.TextBox textBoxdhcpChanges;
        private System.Windows.Forms.Label labeldhcpChanges;
        private System.Windows.Forms.TextBox textBoxRandomNumber;
        private System.Windows.Forms.TextBox textBoxRestarted;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelPersonalMessage;
    }
}

